from arcgis.gis import GIS
import getpass
gis = GIS('https://arcgis.com', 'NEGEO_ADMIN', getpass.getpass())
for folder in gis.users.me.folders: 
    if folder['title'] == 'AERIAL_CACHE' or folder['title'] == 'BASEMAP_CACHE':
        print(folder['title'], end='')
        itemsInFolders = gis.users.me.items(folder['title'], max_items=400) 
        print(f': has {len(itemsInFolders)} items')
        for i in itemsInFolders:
            print(f"{i.title}")
