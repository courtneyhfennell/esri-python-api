#connect to GIS
import pandas as pd

from arcgis.gis import GIS
from IPython.display import display

#Configuration

#AGOL login
username = "clewis"
password = "password"
#Full path to file
vaccination_file = r'C:\Users\clewis\Downloads\Percent Initiated Vaccination (9-13-21).xlsx'
service_ID = "73485092b5864e7f8c74b6b341948fe2" #found from url https://norwalk.maps.arcgis.com/home/item.html?id=73485092b5864e7f8c74b6b341948fe2

gis = GIS("https://norwalk.maps.arcgis.com/home",username, password)

#search for the feature layer
Percent_Initiated_Vaccination = gis.content.get(service_ID)

#access the item's feature layers
# returns a list of all the layers - in this case there is only 1
all_layers = Percent_Initiated_Vaccination.layers 

feature_set = all_layers[0].query()

df = pd.read_excel(vaccination_file, header=2, na_filter=False)

for tuple_row in df.iterrows():
    row =tuple_row[1]
    feat_set = all_layers[0].query(where=f"Neighborhood='{row['Neighborhood']}'")
    neighborhood_feature = feat_set.features[0]

    edit_feature = neighborhood_feature

    edit_feature.attributes["Aged_16_"] = row['Aged 16+']
    edit_feature.attributes["Aged_16_44"] = row['Aged 16-44']
    edit_feature.attributes["Aged_45_64"] = row['Aged 45-64']
    edit_feature.attributes["Aged_65_"] = row['Aged 65+']
    
    edit_feature.attributes["High_SVI"] = row['High SVI']
    
    edit_feature.attributes["Sum_of___of_16_"] = row['Sum of # of 16+']
    edit_feature.attributes["Sum_of___of_16_44"] = row['Sum of # of 16-44']
    edit_feature.attributes["Sum_of___of_45_64"] = row['Sum of # of 45-64']
    edit_feature.attributes["Sum_of___of_65_"] = row['Sum of # of 65+']

    update_result = all_layers[0].edit_features(updates=[edit_feature])
    
print("done")