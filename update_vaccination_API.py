#connect to GIS
import pandas as pd

from sodapy import Socrata
from arcgis.gis import GIS
from IPython.display import display

#Configuration

#AGOL login
username = "username"
password = "password"
#Full path to file
service_ID = "73485092b5864e7f8c74b6b341948fe2" #found from url https://norwalk.maps.arcgis.com/home/item.html?id=73485092b5864e7f8c74b6b341948fe2

gis = GIS("https://norwalk.maps.arcgis.com/home", username, password)

#search for the feature layer
Percent_Initiated_Vaccination = gis.content.get(service_ID)

#access the item's feature layers
# returns a list of all the layers - in this case there is only 1
all_layers = Percent_Initiated_Vaccination.layers 
feature_set = all_layers[0].query()


# Unauthenticated client only works with public data sets. Note 'None'
# in place of application token, and no username or password:
client = Socrata("data.ct.gov", None)
results = client.get("fj4n-gyni", query="SELECT * WHERE Town='Norwalk' ORDER BY dateupdated DESC limit 22") #https://dev.socrata.com/foundry/data.ct.gov/gngw-ukpw

# Convert to pandas DataFrame
results_df = pd.DataFrame.from_records(results)
print(results_df)
for tuple_row in results_df.iterrows():
    row =tuple_row[1]
    feat_set = all_layers[0].query(where=f"GEOID='{row['geo_id10']}'")
    neighborhood_feature = feat_set.features[0]

    edit_feature = neighborhood_feature

    edit_feature.attributes["Aged_16_"] = row['sixteen_plus']
    edit_feature.attributes["Aged_16_44"] = row['sixteen_fortyfour']
    edit_feature.attributes["Aged_45_64"] = row['fortyfive_sixtyfour']
    edit_feature.attributes["Aged_65_"] = row['sixtyfive_plus']
    
    #edit_feature.attributes["High_SVI"] = row['High SVI']
    
    edit_feature.attributes["Sum_of___of_16_"] = row['sixteen_plus_n']
    edit_feature.attributes["Sum_of___of_16_44"] = row['sixteen_fortyfour_n']
    edit_feature.attributes["Sum_of___of_45_64"] = row['fortyfive_sixtyfour_n']
    edit_feature.attributes["Sum_of___of_65_"] = row['sixtyfive_plus_n']

    update_result = all_layers[0].edit_features(updates=[edit_feature])
    
print("done")