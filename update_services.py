# -------------------------------------------------------------------------------
#
# Author:      Courtney Fennell
#
# -------------------------------------------------------------------------------


# -------------------------------------------------------------------------------
def update_services(out_folder_path=None):
    import logging
    import arcpy_utils

    from arcgis.gis import GIS
    from arcgis.features import FeatureLayerCollection

    import os
    import datetime
    import arcpy

    excel_path = r"D:\PythonDOT\Seasonal_Weight_Restrictions"
    if not out_folder_path:
        out_folder_path = arcpy_utils.get_RH_log_folder()

    if not os.path.exists(out_folder_path):
        os.mkdir(out_folder_path)
    log_file = out_folder_path + datetime.datetime.today().strftime('\\WeightRestrictions-%Y-%m-%d.log')
    counter = 1
    while os.path.exists(log_file):
        log_file = out_folder_path + datetime.datetime.today().strftime(f'\\WeightRestrictions-%Y-%m-%d_{counter}.log')
        counter += 1
    logging.basicConfig(
        filename=log_file,
        filemode='w',
        format='[%(asctime)s] %(levelname)-6s - %(funcName)s : %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

    logger = logging.getLogger("")
    logger.setLevel(logging.DEBUG)
    logger.info("Logging")

    add_features = f"add_features"
    hosted_layer = "https://services.arcgis.com/r4A0V7UzH9fcLVvv/arcgis/rest/services/SeasonalWeightRestrictions/FeatureServer"
    remove_features = "remove_features"
    local_layer = arcpy_utils.report_sde + arcpy_utils.report_prefix + "APP_SeasonalWeightRestrictions_Prod"
    print(hosted_layer)
    for root, dirs, files in os.walk(excel_path):
        for file in files:
            #if file.endswith(".xlsx") and file.strip(".xlsx")[2:] == datetime.datetime.today().strftime("%m-%d-%Y"):
            if file.endswith(".xlsx") and file.split('_')[2].strip(".xlsx") == datetime.datetime.today().strftime("%m-%d-%Y"):
                print(f"Running {file}")
                # ArcGIS automatically changes - to _
                in_table = f"{arcpy_utils.scratch_gdb()}\\{file.split('.')[0].replace('-', '_')}"
                # get new features from spreadsheet
                arcpy.ExcelToTable_conversion(f"{root}\\{file}", in_table)
                logger.debug("ExcelToTable_conversion complete")
                
                # Delete all unacceptable values
                arcpy.MakeTableView_management(in_table, remove_features)
                arcpy.SelectLayerByAttribute_management(remove_features, "NEW_SELECTION", f"Weight_Restriction is NULL")
                logger.debug("SelectLayerByAttribute_management complete")

                if int(arcpy.GetCount_management(remove_features)[0]) > 0:
                    print(int(arcpy.GetCount_management(remove_features)[0]))
                    arcpy.DeleteRows_management(remove_features)

                table_props = "Route_ID Line From_Measure To_Measure"
                print(in_table)
                

                arcpy.lr.MakeRouteEventLayer(arcpy_utils.report_sde + arcpy_utils.report_prefix + "LRSN_Routes",
                     "Route_ID", in_table,
                     table_props, add_features,
                     None, "NO_ERROR_FIELD", "NO_ANGLE_FIELD", "NORMAL", "ANGLE", "LEFT", "POINT")
                logger.debug("MakeRouteEventLayer_lr complete")

                # Remove features from specified region
                arcpy.MakeTableView_management(local_layer, remove_features)
                logger.debug("MakeTableView_management complete")
                if file.split('_')[0] == "NR":
                    key = 'Northern Region'
                elif file.split('_')[0] == "SR":  # .strip(".xlsx")[0:2]
                    key = 'Southcoast Region'
                elif file.split('_')[0] == "CR":
                    key = 'Central Region'
                arcpy.SelectLayerByAttribute_management(remove_features, "NEW_SELECTION", f"Region = '{key}' or Region is NULL")
                logger.debug("SelectLayerByAttribute_management complete")

                if int(arcpy.GetCount_management(remove_features)[0]) > 0:
                    print(f"Removing {arcpy.GetCount_management(remove_features)[0]} features")
                    arcpy.DeleteRows_management(remove_features)
                    logger.debug("DeleteRows_management complete")
                #arcpy.FeatureClassToFeatureClass_conversion(add_features, arcpy_utils.report_sde, "APP_SeasonalWeightRestrictions")
                field_mappings = arcpy.FieldMappings()
                field_mappings.addTable(local_layer)
                field_mappings.addTable(add_features)

                # for in_field, out_field in zip(fields, named_zones_fields):
                    # field_to_map_index = field_mappings.findFieldMapIndex(out_field)
                    # field_to_map = field_mappings.getFieldMap(field_to_map_index)
                    # field_to_map.addInputField(append_layer, in_field)
                    # field_mappings.replaceFieldMap(field_to_map_index, field_to_map)
                arcpy.Append_management([add_features], local_layer, schema_type ="NO_TEST",)
                #arcpy.SelectLayerByAttribute_management(local_layer, "NEW_SELECTION", "'Weight_Restriction' NOT IN ('100', '75', '85', '85LCV', '50')")
                #logger.debug("SelectLayerByAttribute_management complete")

                #if int(arcpy.GetCount_management(local_layer)[0]) > 0:
                #    print(f"Removing {arcpy.GetCount_management(local_layer)[0]} features")
                #    arcpy.DeleteRows_management(local_layer)
                #portal = GIS("https://akdot.maps.arcgis.com/home", "chlewis2_AKDOT", "")

                # POINT TOWARDS LOCAL LAYER THAT NEEDS UPLOAD
                #local_sde = arcpy_utils.report_sde

                # FIND FEATURE SERVICE USING IT'S ID
                #hosted_layer = portal.content.get("aa60b18a08c44f3688e3c884b459d2f2")
                #flc_Points_ONLINE = FeatureLayerCollection.fromitem(hosted_layer)

                # OVERWRITE FEATURE SERVICE
                #flc_Points_ONLINE.manager.overwrite(r"U:\DBConnections\REPORTGDB_Def_DOTREPORT.sde\REPORTGDB.DOTREPORT.APP_SeasonalWeightRestrictions")

                # This can be used to delete selected features from a layer.
                # hosted_layer_featurelayer = hosted_layer.layers[0]
                # hosted_layer_featureset = hosted_layer_featurelayer.query()
                # SWR_features = hosted_layer_featureset.features
                # hosted_layer_attr = [f for f in SWR_features if f.attributes['Region'] == 'Northern Region']
                # for each_attr in hosted_layer_attr:
                #     region_ObjectID = each_attr.get_value('OBJECTID')
                #     # pass the object id as a string to the delete parameter
                #     hosted_layer_featurelayer_res = hosted_layer_featurelayer.edit_features(
                #         deletes=str(region_ObjectID))
    print("\n\nScript completed without errors. \n\n")

if __name__ == '__main__':
    import arcpy_utils

    update_services(arcpy_utils.get_RH_log_folder())
